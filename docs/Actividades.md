# **Capacidad y ruido del canal**

## *Trabajo Práctico N°3*

*Realizado por Emanuel Gauler*

###  Actividad 1. Diferencia entre intermodulación y diafonía

El ruido de intermodulación es cuando una señal externa a la señal transmitida, con frecuencias armónicas a la transmitida, interfiere en la señal transmitida.
Por lo que la señal externa distorciona la señal transmitida.

Por otro lado la diafonía, es el acoplamiento de señales en un mismo medio de transmisión. Puede ser 2(dos) pares tranzados dentro de un mismo revestimiento.

### Actividad 2. Cálculo de la cantidad de ruido térmico para 1MHz, 1KHz y 1GHz, con 15°C

|  Ancho de banda |  Cantidad de ruido |
|  :-             |                 -: |
|  1KHz           |  -174 dBW          |
|  1MHz           |  -144 dBW          |
|  1GHz           |  -115 dBW          |

### Actividad 3. Capacidad del canal y niveles de modulación

|  SNR   |  Capacidad del canal  |  Niveles de modulación   |
|  :-    |           :-:         |                       :- |
|  500   |  27.802 bps           |  16                      |
|  1000  |  30.898 bps           |  32                      |
|  2000  |  33.996 bps           |  32                      |
|  5000  |  38.092 bps           |  64                      |
|  10000 |  41.192 bps           |  128                     |

### Actividad 4. Capacidad del canal y niveles de señalización

|  SNR   |  Niveles  de señalización   |
|  :-    |                          :- |
|  500   |  16                         |
|  1000  |  32                         |
|  5000  |  64                         |

Solo muestro la SNR y los niveles de señalización por que encontré que el ancho de banda no influye en los niveles de señalización con respecto a la relación señal-ruido.

### Actividad 5. 

|  Velocidades de transmisión |  Niveles de señalización |
|  2400 bps                   |  -161,8 dBW              |
|  9600 bps                   |  -155,7 dBW              |
|  38400 bps                  |  -149,7 sBW              |

### Actividad 6.

El nivel de ruido a la salida, para 5.000°K con un ancho de banda de 8MHz, es -122,58 dBW.

### Actividad 7.

```
No = -228,6 + 10\*log(35+273,15) + 10\*log(10⁶)
No = -228,6 + 24,89 + 60
No = -143,71 dBW
```

### Actividad 8. 

Para calcular la velocidad máxima ideal, se utiliza la expresión de Nyquist, que indica que la velocidad de transferencia es el doble del ancho de banda.

`C = 2 \* W. C = 2 \* 5MHz => C = 10Mbps`

### Actividad 9.

La velocidad máxima en la que se puede transmitir es 3Mbps, según la fórmula de Nyquist, `2 \* 1,5MHz = 3Mbps`

### Actividad 10.

```
C = B log2( 1+SNR ) => SNR = 2^(C/B) - 1

SNR = 2^(2Gbps/10MHz) - 1 => SNR = 160693804425...
```
El SNR es un número muy grande. No es aplicable.

### Actividad 11.

```
C = W log2( 1 + SNR )
SNR = 10^( SNRdb / 10 ) => SNR = 10^( 35/10 ) = 10^( 3,5 ) = 3162,27 = SNR
4Gbps = 3MHz * log2( 1 + 3162,27 )
4Gbps = 24.178.084,74 bps
4Gbps > 24,178 Mbps
```

El sistema diseñado no alcanza la capacidad deseada. El sistema solo puede alcanzar hasta 24,178 Mbps.

### Actividad 12.

Para un sistema que codifica una señal de 4 bits. El ancho de banda necesario sería de 800 Hz.
```
C = 2W log2( M )
3,2 Kbps = 2W log2( 4 )
3,2 Kbps = 2W * 2
3,2 Kbps = 4W
0,8 Hz   = W
800 Hz   = W
```

Para un sistema que codifica una señal de 8 bits, el ancho de banda necesario es de 2,13 KHz.
```
3,2 Kbps = 2W log2( 8 )
3,2 Kbps = 2W * 3
3,2 Kbps = 6W
2,13 KHz = W
```

### Actividad 13.

Para transmitir un vídeo a 512 Kbps con SNR=60dB, se necesita un ancho de banda de ~25 KHz.
```
60db = 6 * 10 log(10) = 10 log( 10⁶ ) => SNR = 10⁶
512 Kbps    = W log2( 1 + 10⁶ )
512 Kbps    = 19,93 W
25,68 KHz   = W
```
