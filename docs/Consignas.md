# **Actividades a realizar**

1.	Explicar la diferencia entre ruido de intermodulación y diafonía.

2.	Calcular la cantidad de ruido térmico en un conductor para los siguientes anchos de banda, 1MHz, 1KHz y 1GHz.

3.	Un canal de voz y las siguientes relaciones S/N: 500, 1000, 2000, 5000, 10000calcular para c/u de las S/N, el límite de Shannon y la cantidad de niveles de modulación necesarios para la tasa de bits calculada.

4.	Calcular la capacidad de un canal y el número de niveles de señalización necesarios, considenrando las fórmulas de Shannon y Nyquist, para los siguientes datos: relación señal-ruido de 500:1, 1000:1, 5000:1; ancho de banda: 10 MHz, 100MHz.

5.	Calcular qué niveles de señal es necesario recibir para las siguientes velocidades de transmisión: 2400bps, 9600bps, 38400bps; si la probabilidad de error en un bir es 10⁻⁴, eb/no = 8,4db; t = 290°K.

6.	Dada una temperatura efectiva de ruido de 5.000°K, en un sistema cuyo ancho de banda es 8MHz, calcular el nivel de ruido a la salida.
	
    Nota. Exprese el resultado en Watts y en decibelios.

7.	Calcule la relación señal/ruido(SNR) en decibelios para un canal de ancho de banda de 10MHz, siendo la potencia de entrada 1.500Watts, si opera a una temperatura de 35°C.

8.	Si se quiere transmitir señales binarias por un canal de 5MHz (sin considerar el ruido). Calcular la velocidad máxima a la que se puede transmitir.

9.	Según Nyquist para un canal ideal sin ruido es:
 `C = 2 B log2( M )`
 
 	Dado un canal con ancho de banda de 1,5MHz y una relación de señal ruido térmico SNR = 65dB.
	
    Calcular la velocidad máxima a la que se puede transmitir.

10.	Según Shannon para un canal real con ruido es: 

	`C = B log2( 1+SNR )`
   
   	Sea un canal con una capacidad de 2Gbps y un ancho de banda de 10MHz; calcule la relación señal-ruido admisible para conseguir la mencionada capacidad.

11.	Se ha diseñado un sistema de comunicaciones que emplea un ancho de banda 3MHz y en que se consigue una SNR = 35dB. Demuestre que el sistema puede alcanzar los 4Gbps.

12.	Para operar a 3,2Kbps se usa un sistema de señalización digital.
	
    Si cada elemento de señal codifica una palabra de 4 bits, calcular el ancho de banda mínimo necesario. Ídem para palabras de 8 bits.

13.	Considere que un video se debe transmitir a través de un canal SNR = 60dB. Determinar el ancho de banda mínimo del canal sabiendo el vídeo se digitaliza a 512Kbps.
